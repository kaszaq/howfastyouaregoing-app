package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp;

import pl.kaszaq.howfastyouaregoing.agile.AgileProject;

import java.time.LocalDateTime;
import java.util.SortedMap;

public interface ProjectStorage {

     SortedMap<String, ProjectStatus> getAvailableProjects();

     AgileProject getProject(String projectId);
}
