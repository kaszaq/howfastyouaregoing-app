/*
 * Copyright 2017 Michal Kasza.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.io.File;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.kaszaq.howfastyouaregoing.Config;
import pl.kaszaq.howfastyouaregoing.agile.AgileProject;
import pl.kaszaq.howfastyouaregoing.agile.AgileProjectConfiguration;
import pl.kaszaq.howfastyouaregoing.agile.AgileProjectProvider;
import pl.kaszaq.howfastyouaregoing.agile.jira.JiraAgileProjectProviderBuilderFactory;
import pl.kaszaq.howfastyouaregoing.storage.FileStorage;

@Slf4j
@Service
public class ProjectKeeper implements InitializingBean {

    private final AgileProjectConfiguration agileProjectConfiguration = AgileProjectConfiguration.builder().build();
    private final Map<String, Double> loadingProjects = new ConcurrentHashMap<>();
    Set<String> loadedProjects = Collections.synchronizedSet(new HashSet<>());
    Map<String, LocalDateTime> projectsLastUpdated = new ConcurrentHashMap<>();
    @Value("${jira.url}")
    private String jiraUrl;
    @Autowired
    FileStorage fileStorage;
    @Autowired
    JiraProjects jiraProjects;
    @Value("${jira.username:#{null}}")
    private String jiraUserName;
    @Value("${jira.password:#{null}}")
    private String jiraPassword;

    @Autowired
    private BasicAuthHeader basicAuthHeader;

    @Value("${storage.dir}")
    private String storageDir;
    private File cacheDir;
    private LoadingCache<String, AgileProject> projectsCache;

    @PostConstruct
    public void setupCache() {
        System.out.println("Storage dir: " + storageDir);
        cacheDir = new File(storageDir);
        System.out.println("cache dir: " + cacheDir);
    }

    public SortedMap<String, Double> getLoadingProjects() {
        TreeMap<String, Double> tempLoadingProjects = new TreeMap<>();
        for (String projectKey : jiraProjects.getProjectsAvailableForUser()) {
            if(!loadedProjects.contains(projectKey)){
                tempLoadingProjects.put(projectKey, -1.0);
            }
        };
        tempLoadingProjects.putAll(loadingProjects);
        return tempLoadingProjects;
    }

    public SortedMap<String, LocalDateTime> getLoadedProjects() {
        TreeMap<String, LocalDateTime> map = new TreeMap<>(projectsLastUpdated);
        map.keySet().retainAll(loadedProjects);
        return map;
    }

    public AgileProject getProject(String projectId) {
        return projectsCache.getUnchecked(projectId);
    }

    public void loadProject(String projectId) {
        loadProjectUpperCased(projectId.toUpperCase());
    }

    private void loadProjectUpperCased(String projectId) {
        synchronized (loadingProjects) {
            Double projectLoadPerc = loadingProjects.get(projectId);
            if (projectLoadPerc != null && projectLoadPerc >= 0.0) {
                return;
            }
            loadingProjects.put(projectId, 0.0);
        }
        try {
            checkIfUserCanLoadProject(projectId);
        } catch (Exception ex) {
            loadingProjects.remove(projectId);
            throw ex;
        }
        AgileProjectProvider agileProjectProvider = getJiraAgileProjectProviderBuild()
                .withCacheDir(cacheDir)
                .withFileStorage(fileStorage)
                .withMinutesUntilUpdate(0)
                .withJiraUrl(jiraUrl)
                .withEmptyDescriptionAndSummary(true)
                .withCacheRawJiraFiles(false)
                .build();

        CompletableFuture<Optional<AgileProject>> future = CompletableFuture.supplyAsync(() -> {
            return agileProjectProvider.loadProject(projectId, agileProjectConfiguration, (project, progress) -> {
                loadingProjects.put(projectId, progress);
                projectsLastUpdated.put(projectId, project.getLastUpdated().toLocalDateTime());
            });
        });
        future.thenAccept(projectOptional -> {
            if (projectOptional.isPresent()) {
                AgileProject project = projectOptional.get();
                loadedProjects.add(projectId);
            }

            loadingProjects.remove(projectId);
            projectsCache.invalidate(projectId);

        });
    }

    private JiraAgileProjectProviderBuilderFactory.JiraAgileProjectProviderBuilder getJiraAgileProjectProviderBuild() {
        if (jiraUserName != null && jiraPassword != null) {
            return JiraAgileProjectProviderBuilderFactory.withCredentials(jiraUserName, jiraPassword);
        } else {
            return JiraAgileProjectProviderBuilderFactory
                    .withCredentials((String) Optional.ofNullable(SecurityContextHolder.getContext())
                                    .map(SecurityContext::getAuthentication)
                                    .filter(a -> a.getClass().equals(UsernamePasswordAuthenticationToken.class))
                                    .map(Authentication::getPrincipal).orElse(null),
                            (String) Optional.ofNullable(SecurityContextHolder.getContext())
                                    .map(SecurityContext::getAuthentication)
                                    .filter(a -> a.getClass().equals(UsernamePasswordAuthenticationToken.class))
                                    .map(Authentication::getDetails).orElse(null));
        }
    }

    private void checkIfUserCanLoadProject(String projectId) {
        ArrayList<BasicHeader> header = basicAuthHeader.get();
        try (CloseableHttpClient client = HttpClients.custom()
                .useSystemProperties()
                .setDefaultHeaders(header)
                .build()) {
            HttpGet httpGet = new HttpGet(jiraUrl + "/rest/api/2/mypermissions?permissions=BROWSE_PROJECTS&projectKey=" + projectId);
            try (CloseableHttpResponse response = client.execute(httpGet)) {
                final int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 404) {
                    throw new NotFoundException();
                }
                if (statusCode != 200) {
                    throw new AccessDeniedException("Unable to verify your permissions in jira. Jira returned http status " + statusCode);
                }
                String responseValue = EntityUtils.toString(response.getEntity(), "UTF-8");
                if (!Config.OBJECT_MAPPER.readTree(responseValue).get("permissions").get("BROWSE_PROJECTS").get("havePermission").asBoolean()) {
                    throw new AccessDeniedException("You do not have sufficient permissions to browse project " + projectId);
                }
            }
        } catch (Exception ex) {
            log.error("Problem while closing? http client", ex);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        AgileProjectProvider localProjectProvider = JiraAgileProjectProviderBuilderFactory
                .withJsession(null)
                .withCacheDir(cacheDir)
                .withFileStorage(fileStorage)
                .withCacheOnly(true)
                .withJiraUrl(jiraUrl)
                .withEmptyDescriptionAndSummary(true)
                .withCacheRawJiraFiles(false)
                .build();
        Stream.<File>of(cacheDir
                .listFiles((File file) -> file.isFile() && file.getName().endsWith(".json")))
                .map(file -> file.getName().substring(0, file.getName().indexOf(".json")))
                .forEach(projectId -> {
                    log.info("Initial loading project {} from file system.", projectId);
                    final Optional<AgileProject> projectFromFile = localProjectProvider.loadProject(projectId, agileProjectConfiguration, (project, progress) -> {
                        projectsLastUpdated.put(projectId, project.getLastUpdated().toLocalDateTime());
                    });
                    if (projectFromFile.isPresent()) {
                        loadedProjects.add(projectId);
                    } else {
                        log.warn("Initial loading project {} from file system failed.", projectId);
                    }
                });


        projectsCache = CacheBuilder.newBuilder().expireAfterAccess(120, TimeUnit.SECONDS)
                .build(new CacheLoader<String, AgileProject>() {
                    @Override
                    public AgileProject load(String projectId) {
                        AgileProjectProvider localProjectProvider = JiraAgileProjectProviderBuilderFactory
                                .withJsession(null)
                                .withCacheDir(cacheDir)
                                .withFileStorage(fileStorage)
                                .withCacheOnly(true)
                                .withJiraUrl(jiraUrl)
                                .withEmptyDescriptionAndSummary(true)
                                .withCacheRawJiraFiles(false)
                                .build();
                        log.info("Initial loading project {} from file system.", projectId);
                        final Optional<AgileProject> projectFromFile = localProjectProvider.loadProject(projectId, agileProjectConfiguration);
                        if (projectFromFile.isPresent()) {
                            return projectFromFile.get();
                        } else {
                            log.warn("Initial loading project {} from file system failed.", projectId);
                            throw new RuntimeException("Initial loading project from file system failed: " + projectId);
                        }
                    }
                });
    }

    @Scheduled(fixedRate = 5 * 1000 * 60)
    public void reportCurrentTime() {
        if (jiraUserName != null && jiraPassword != null) {
            log.info("Scheduler checking projects");
            Optional<String> project = this.getLoadedProjects().entrySet().stream()
                    .filter(e -> !loadingProjects.containsKey(e.getKey()))
                    .filter(e -> e.getValue().isBefore(
                            LocalDateTime.now().minusHours(6)
                    ))
                    .sorted((e1, e2) -> e1.getValue().compareTo(e2.getValue()))
                    .map(e -> e.getKey()).findFirst();
            project.ifPresent(id -> {
                this.loadProject(id);
                log.info("Scheduler loading project {}", id);
            });
        }

    }

}
