package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.datapoint;

import java.time.LocalDate;

public class LocalDateDataPoint {
    private final LocalDate x;
    private final Double y;

    public LocalDateDataPoint(LocalDate x, Double y) {
        this.x = x;
        this.y = y;
    }

    public LocalDate getX() {
        return x;
    }

    public Double getY() {
        return y;
    }
    
}
