package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.LineChart;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.color.Color;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.data.LineData;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.dataset.LineDataset;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.LineOptions;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.Title;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.Tooltips;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.elements.LineElements;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.elements.Point;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.scales.LinearScale;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.scales.LinearScales;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.scales.ScaleLabel;
import java.time.format.DateTimeFormatter;

public class GraphBuilder {

    private static final String[] COLORS = new String[]{"#3366CC", "#DC3912", "#FF9900", "#109618", "#990099",
        "#3B3EAC", "#0099C6", "#DD4477",
        "#66AA00", "#B82E2E", "#316395", "#994499", "#22AA99", "#AAAA11", "#6633CC", "#E67300", "#8B0707",
        "#329262", "#5574A6", "#3B3EAC"};
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static LineChart constructGraph(String nameOfGraph, List<LocalDate> labels, Map<String, ? extends List<? extends Number>> values,
            boolean isStacked) {
        return constructGraph(nameOfGraph, labels, values, isStacked, true);
    }

    public static LineChart constructGraph(String nameOfGraph, List<LocalDate> labels, Map<String, ? extends List<? extends Number>> values,
            boolean isStacked, boolean interpolateLines) {

        LineChart chart = new LineChart()
                .setData(new LineData()
                        .addLabels(labels.stream().map(v -> {
                            try {
                                return FORMATTER.format(v);
                            } catch (Exception ex) {
                                throw new RuntimeException(ex);
                            }
                        }).toArray(String[]::new)
                        )
                )
                .setOptions(new LineOptions()
                        .setTooltips(new Tooltips()
                                .setMode("index")
                                .setIntersect(false)
                        )
                        .setElements(new LineElements()
                                .setPoint(new Point()
                                        .setHitRadius(2)
                                        .setHoverRadius(5)
                                        .setRadius(0)
                                )
                        )
                        .setResponsive(true)
                        .setTitle(new Title()
                                .setDisplay(true)
                                .setText(nameOfGraph)
                        )
                        .setScales(new LinearScales()
                                .addxAxis(new LinearScale()
                                        .setType("time")
                                        .setDisplay(true)
                                        .setScaleLabel(new ScaleLabel()
                                                .setDisplay(true)
                                                .setLabelString("Date")
                                        )
                                )
                                .addyAxis(new LinearScale()
                                        .setStacked(isStacked)
                                        .setDisplay(true)
                                        .setScaleLabel(new ScaleLabel()
                                                .setDisplay(true)
                                                .setLabelString("Value")
                                        )
                                )
                        )
                );
        int colorNo = 0;
        for (Map.Entry<String, ? extends List<? extends Number>> entry : values.entrySet()) {
            Color color = new Color(COLORS[colorNo++ % COLORS.length]);
            chart.getData().addDataset(new LineDataset()
                    .setLabel(entry.getKey())
                    .setType("line")
                    .setBackgroundColor(color)
                    .setBorderColor(color)
                    .setFill(isStacked) //'origin'
                    .setBorderWidth(1)
                    .setLineTension(interpolateLines ? null : 0f)
                    .setData(entry.getValue())
            );
        }
        return chart;

    }

}
