package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.kaszaq.howfastyouaregoing.Config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class JiraProjects {
    @Value("${jira.url}")
    private String jiraUrl;
    @Autowired
    private BasicAuthHeader basicAuthHeader;
    @Autowired
    UsersProjectsList usersProjectsList;

    public Set<String> getProjectsAvailableForUser(){
        if(usersProjectsList.getProjectKeys()!=null){
            return usersProjectsList.getProjectKeys();
        }

        try (CloseableHttpClient client = HttpClients.custom()
                .useSystemProperties()
                .setDefaultHeaders(basicAuthHeader.get())
                .build()) {
            HttpGet httpGet = new HttpGet(jiraUrl + "/rest/api/2/project");
            try (CloseableHttpResponse response = client.execute(httpGet)) {
                final int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 404) {
                    throw new NotFoundException();
                }
                if (statusCode != 200) {
                    throw new AccessDeniedException("Unable to verify your permissions in jira. Jira returned http status " + statusCode);
                }
                String responseValue = EntityUtils.toString(response.getEntity(), "UTF-8");
                Set<String> projectKeys = new HashSet<>();
                for (JsonNode projectNode : Config.OBJECT_MAPPER.readTree(responseValue)) {
                    projectKeys.add(projectNode.get("key").asText());
                }
                usersProjectsList.setProjectKeys(projectKeys);
                return projectKeys;
            }
        } catch (Exception ex) {
            log.error("Problem while closing? http client", ex);
            throw new RuntimeException(ex);
        }
    }
}
