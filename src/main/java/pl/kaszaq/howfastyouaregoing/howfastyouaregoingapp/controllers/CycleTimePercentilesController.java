package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.controllers;

import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.Chart;
import com.fasterxml.jackson.core.JsonProcessingException;
import static com.google.common.collect.Lists.newArrayList;

import java.io.IOException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Predicate;
import org.apache.commons.lang3.ObjectUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.kaszaq.howfastyouaregoing.agile.AgileProject;
import pl.kaszaq.howfastyouaregoing.agile.Issue;
import static pl.kaszaq.howfastyouaregoing.agile.IssuePredicates.inStatus;
import pl.kaszaq.howfastyouaregoing.agile.IssueStatusTransition;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.Filters;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.GraphBuilder;

import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.ProjectKeeper;


@RestController
public class CycleTimePercentilesController {

    @Autowired
    ProjectKeeper projectKeeper;

    @CrossOrigin(origins = "*")
    @RequestMapping("/cycleTimePercentilesController")
    public Chart getHfyag(
            @RequestParam(value = "projectId") String projectId,
            @RequestParam(value = "doneStatuses") Set<String> doneStatuses,
            @RequestParam(value = "inProgressStatuses") String[] inProgressStatuses,
            Filters filters
    ) throws JsonProcessingException, IOException {

        String[] wipStatuses = inProgressStatuses;

        Predicate<Issue> commonFilters = filters.getFilter();

        AgileProject project = projectKeeper.getProject(projectId);
        LocalDate sinceDate = ObjectUtils.max(filters.getSinceDate(), project.getFirstIssueCreateDate().toLocalDate());
        LocalDate till = filters.getTillDate();

        return doTheMagic(project, doneStatuses, sinceDate, till, filters.getRollingAverage(), wipStatuses, commonFilters);
    }

    private Chart doTheMagic(AgileProject agileProject, Set<String> finalStatus, final LocalDate since, final LocalDate till, int daysAverage, String[] wipStatuses, Predicate<Issue> commonFilters) throws IOException {

        List<LocalDate> labels = new ArrayList<>();
        Map<String, List<Double>> percentiles = new LinkedHashMap<>();
        percentiles.put("5%", new ArrayList<>());
        percentiles.put("10%", new ArrayList<>());
        percentiles.put("25%", new ArrayList<>());
        percentiles.put("50%", new ArrayList<>());
        percentiles.put("60%", new ArrayList<>());
        percentiles.put("75%", new ArrayList<>());
        percentiles.put("85%", new ArrayList<>());
        percentiles.put("90%", new ArrayList<>());
        percentiles.put("95%", new ArrayList<>());
        percentiles.put("99%", new ArrayList<>());
        SortedMap<LocalDate, List<Double>> cycleTimeStatistics = new TreeMap<>(agileProject.getAllIssues()
                .stream()
                .filter(commonFilters)
                .filter(inStatus(finalStatus))
                .filter(i -> i.getIssueStatusTransitions()
                .stream()
                .anyMatch(st -> finalStatus.contains(st.getToStatus()))
                )
                .reduce(new TreeMap<LocalDate, List<Double>>(), (TreeMap<LocalDate, List<Double>> u, Issue i) -> {
                    LocalDate key = getClosedDate(i, finalStatus);
                    List<Double> list = u.computeIfAbsent(key, k -> newArrayList());
                    list.add(((double) i.getDurationInStatuses(wipStatuses).getSeconds()) / 3600.0 / 24.0);
                    return u;
                }, (TreeMap<LocalDate, List<Double>> u1, TreeMap<LocalDate, List<Double>> u2) -> {
                    u2.forEach((k,v)->{
                        u1.computeIfAbsent(k, (LocalDate k2) -> newArrayList()).addAll(v);
                    });
                    return u1;
                }));

        for (LocalDate k = since; !k.isAfter(till); k = k.plusDays(1)) {
            double[] data = cycleTimeStatistics.subMap(k.minusDays(daysAverage), k.plusDays(1)).values().stream().flatMap(v -> v.stream()).sorted().mapToDouble(d -> d).toArray();
            percentiles.get("5%").add(getPercentile(data, 5.0));
            percentiles.get("10%").add(getPercentile(data, 10.0));
            percentiles.get("25%").add(getPercentile(data, 25.0));
            percentiles.get("50%").add(getPercentile(data, 50.0));
            percentiles.get("60%").add(getPercentile(data, 60.0));
            percentiles.get("75%").add(getPercentile(data, 75.0));
            percentiles.get("85%").add(getPercentile(data, 85.0));
            percentiles.get("90%").add(getPercentile(data, 90.0));
            percentiles.get("95%").add(getPercentile(data, 95.0));
            percentiles.get("99%").add(getPercentile(data, 99.0));
            labels.add(k);

        }

        return GraphBuilder.constructGraph(
                "Cycle time percentiles",
                labels,
                percentiles,
                false);

    }

    private static Double getPercentile(double[] data, double percentile) {
        int n = data.length;
        if (n == 0) {
            return null;
        }
        if (n == 1) {
            return data[0];
        }
        double pos = percentile / 100.0 * n;
        if (pos >= n - 1) {
            return data[n - 1];
        }
        int floorPos = (int) pos;
        double diff = pos - floorPos;
        return data[floorPos] + diff * (data[floorPos + 1] - data[floorPos]);

    }



    private LocalDate getClosedDate(Issue i, Set<String> finalStatuses) {
        LocalDate returnDate = null;
        boolean previousWasClosed = false;
        for (IssueStatusTransition issueStatusTransition : i.getIssueStatusTransitions()) {
            if (!previousWasClosed && (finalStatuses.contains(issueStatusTransition.getToStatus()))) {
                returnDate = issueStatusTransition.getDate().toLocalDate();
                previousWasClosed = true;
            } else if (!finalStatuses.contains(issueStatusTransition.getToStatus())) {
                previousWasClosed = false;
            }
        }
        return returnDate;
    }
}
