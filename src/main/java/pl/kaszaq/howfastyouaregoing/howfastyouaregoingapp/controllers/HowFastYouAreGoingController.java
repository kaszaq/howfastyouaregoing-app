package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.controllers;

import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.Chart;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableMap;
import static com.google.common.collect.Lists.newArrayList;

import java.io.IOException;
import java.time.DayOfWeek;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.val;
import org.apache.commons.lang3.ObjectUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.kaszaq.howfastyouaregoing.agile.AgileProject;
import pl.kaszaq.howfastyouaregoing.agile.Issue;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.Filters;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.NumberUtils;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.GraphBuilder;

import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.ProjectKeeper;

import pl.kaszaq.howfastyouaregoing.cfd.CfdData;
import pl.kaszaq.howfastyouaregoing.cfd.CfdDataComputer;
import pl.kaszaq.howfastyouaregoing.cycletime.CycleTimeComputer;
import pl.kaszaq.howfastyouaregoing.utils.DateUtils;

@RestController
public class HowFastYouAreGoingController {

    @Autowired
    ProjectKeeper projectKeeper;

    @CrossOrigin(origins = "*")
    @RequestMapping("/hfyagController")
    public Chart getHfyag(
            @RequestParam(value = "projectId") String projectId,
            @RequestParam(value = "doneStatuses") Set<String> doneStatuses,
            @RequestParam(value = "inProgressStatuses") String[] inProgressStatuses,
            Filters filters
    ) throws JsonProcessingException, IOException {
//TODO: this need hardcore refactoring
        //Set<String> doneStatuses = ImmutableSet.copyOf(doneStatuses);
        String[] wipStatuses = inProgressStatuses; //TODO: this should be narrowed by filtered statuses;

        Predicate<Issue> commonFilters = filters.getFilter();
        
        AgileProject project = projectKeeper.getProject(projectId);
        LocalDate sinceDate = ObjectUtils.max(filters.getSinceDate(), project.getFirstIssueCreateDate().toLocalDate());
        LocalDate till = filters.getTillDate();

        return doTheMagic(project, doneStatuses, sinceDate, till, filters.getRollingAverage(), wipStatuses, commonFilters);
    }

    public Chart doTheMagic(AgileProject agileProject, Set<String> finalStatus, final LocalDate since, final LocalDate till, int daysAverage, String[] wipStatuses, Predicate<Issue> commonFilters) throws IOException {
//        Stopwatch timer = null;
        int daysMarkToPlot = 1; // This setting is probably not working as expected
        
        SortedMap<LocalDate, Double> cycleTime = getCycleTime(
                CycleTimeComputer.calulcateCycleTime(agileProject, commonFilters, finalStatus, wipStatuses),
                since,
                till,
                daysAverage,
                daysMarkToPlot);
        
        CfdData data = CfdDataComputer.calculateCfdData(agileProject, commonFilters);

        TreeMap<LocalDate, Map<String, Integer>> cfdData = getCfdData(data);
        SortedMap<LocalDate, Integer> wip = new TreeMap<>();
        for (LocalDate date = since; !date.isAfter(till); date = date.with(addWorkingDays(1))) {
            Map.Entry<LocalDate, Map<String, Integer>> entry = cfdData.floorEntry(date);
            if (entry != null) {
                LocalDate k = entry.getKey();
                Map<String, Integer> v = entry.getValue();
                wip.put(k, Arrays.stream(wipStatuses).mapToInt(status -> v.getOrDefault(status, 0)).sum());
            }
        }
        SortedMap<LocalDate, Double> avgWip = new TreeMap<>();
        double tempVal = 0;
        for (LocalDate k = since; !k.isAfter(till); k = k.with(addWorkingDays(daysMarkToPlot))) {
            OptionalDouble optVal = wip.subMap(k.minusDays(daysAverage), k.plusDays(1)).values().stream().mapToDouble(dailyWip -> new Double(dailyWip)).average();
            if (optVal.isPresent()){
                tempVal = optVal.getAsDouble();
            }
            avgWip.put(k, tempVal);
            
        }
        SortedMap<LocalDate, Integer> throughput = new TreeMap<>();
        SortedMap<LocalDate, CfdData.DailyStatusChanges> dailyTransitions = data.getDailyTransitions();
        for (LocalDate k = since; !k.isAfter(till); k = k.with(addWorkingDays(daysMarkToPlot))) {
            throughput.put(k, dailyTransitions.subMap(k.minusDays(daysAverage), k.plusDays(1)).values().stream()
                    .mapToInt(val -> finalStatus.stream().mapToInt(status -> val.getValueChangeForStatus(status)).sum())
                    .sum());
        }
        return GraphBuilder.constructGraph(
                "How fast you are going",
                newArrayList(cycleTime.keySet()),
                ImmutableMap.of(
                        "Cycle Time", newArrayList(cycleTime.values()),
                        "WIP", newArrayList(avgWip.values()),
                        "Throughput", newArrayList(throughput.values())
                ),
                false);

    }

    private static SortedMap<LocalDate, Double> getCycleTime(SortedMap<LocalDate, Double> cycleTime, LocalDate since, LocalDate till, int daysAverage, int daysMarkToPlot) {

        SortedMap<LocalDate, Double> cycleTimeHours = new TreeMap<>();

        for (LocalDate k = since; !k.isAfter(till); k = k.with(addWorkingDays(daysMarkToPlot))) {
            OptionalDouble valueOptional = cycleTime.subMap(k.minusDays(daysAverage), k.plusDays(1)).values().stream()
                    .mapToDouble(val -> val)
                    .average();
            if (valueOptional.isPresent()) {
                cycleTimeHours.put(k, valueOptional.getAsDouble() / 24);
            } else {
                cycleTimeHours.put(k, null);
            }
        }

        return cycleTimeHours;
    }

    public static TreeMap<LocalDate, Map<String, Integer>> getCfdData(CfdData data) {
        Map<String, Integer> statusValues = new HashMap<>();
        TreeMap<LocalDate, Map<String, Integer>> cfdStatus = new TreeMap<>();
        List<String> statuses = data.getDailyTransitions().values().stream().flatMap(tr -> tr.getStatusChanges().keySet().stream()).distinct().filter(s -> s != null).collect(Collectors.toList());
        data.getDailyTransitions().forEach((k, v) -> {
            HashMap<String, Integer> map = new HashMap<>();
            cfdStatus.put(k, map);
            statuses.forEach(status -> {
                Integer newValue = statusValues.merge(status, v.getValueChangeForStatus(status), Integer::sum);
                map.put(status, newValue);
            });
        });
        return cfdStatus;
    }

    public static TemporalAdjuster addWorkingDays(long workingDays) {
        return TemporalAdjusters.ofDateAdjuster(d -> d.plusDays(workingDays));//addWorkingDay(d, workingDays));//
    }

}
