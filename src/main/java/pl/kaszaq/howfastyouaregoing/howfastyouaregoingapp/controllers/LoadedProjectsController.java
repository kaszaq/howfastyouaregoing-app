/*
 * Copyright 2017 Michal Kasza.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.controllers;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.ProjectKeeper;

@Slf4j
@RestController
public class LoadedProjectsController {

    @Value("${security.enabled}")
    private Boolean securityEnabled;
    @Value("${jira.username:#{null}}")
    private String jiraUserName;
    @Value("${jira.password:#{null}}")
    private String jiraPassword;
    @Autowired
    ProjectKeeper projectKeeper;

    @CrossOrigin(origins = "*")
    @GetMapping("/loadedProjectsController")
    public ProjectKeeper getProjects() {
        return projectKeeper;
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/loadProject")
    public void loadProject(@RequestParam(value = "projectId") String projectId, HttpServletResponse response) throws IOException {
        if (securityEnabled || jiraPassword != null && jiraUserName != null) {
            boolean exc = false;
            try {
                projectKeeper.loadProject(projectId);
            } catch (Throwable ex) {
                log.error("problem while loading project", ex);
                response.sendRedirect("/index.html?error=true");
                exc = true;
            }
            if (!exc) {
                response.sendRedirect("/index.html?loading=true");
            }
        } else {
            response.sendError(401, "You need to be authenticated in order to call this endpoint. Authentication currently not possible as it was disabled.");
        }
    }
}
