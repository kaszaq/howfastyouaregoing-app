package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.controllers;

import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.Chart;
import static com.google.common.collect.Lists.newArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.kaszaq.howfastyouaregoing.cfd.CfdDataComputer;
import java.time.LocalDate;
import java.time.Month;
import static java.time.Month.JANUARY;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.ObjectUtils;
import pl.kaszaq.howfastyouaregoing.agile.AgileProject;
import pl.kaszaq.howfastyouaregoing.agile.Issue;
import static pl.kaszaq.howfastyouaregoing.agile.IssuePredicates.updatedAfter;
import pl.kaszaq.howfastyouaregoing.agile.IssueStatusTransition;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.Filters;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.ProjectKeeper;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.GraphBuilder;
import pl.kaszaq.howfastyouaregoing.cfd.CfdData;
import static pl.kaszaq.howfastyouaregoing.utils.CommonPredicates.alwaysTrue;

@RestController
class CfdController {

    @Autowired
    ProjectKeeper projectKeeper;

    @CrossOrigin(origins = "*")
    @GetMapping("/cfdController")
    public Chart getCfd(@RequestParam(value = "projectId") String projectId,
            @RequestParam(value = "doneStatuses") String[] doneStatuses,
            Filters filters) {
        
        final AgileProject project = projectKeeper.getProject(projectId);
        LocalDate sinceDate = ObjectUtils.max(filters.getSinceDate(), project.getFirstIssueCreateDate().toLocalDate());
        LocalDate tillDate = filters.getTillDate();
        CfdData data = CfdDataComputer.calculateCfdData(project, filters.getFilter());

        HashMap<String, Integer> statusValues = new HashMap<>();

        LinkedHashMap<String, List<Integer>> cfd = new LinkedHashMap<>();
        List<String> statusOrder = project.getAllIssues().stream()
                .filter(updatedAfter(sinceDate.minusMonths(1).atStartOfDay(ZoneId.systemDefault())))
                .flatMap(i -> i.getIssueStatusTransitions().stream()).map(IssueStatusTransition::getToStatus).distinct().collect(Collectors.toList());
        // List<String> statusOrder = data.getDailyTransitions().values().stream().flatMap(tr -> tr.getStatusChanges().keySet().stream()).distinct().filter(s -> s != null).collect(Collectors.toList());
        Stream.of(doneStatuses).forEach(status -> {
            if (statusOrder.remove(status)) {
                statusOrder.add(status);
            }
        });
        statusOrder.forEach(status -> {
            statusValues.put(status, 0);
            cfd.put(status, newArrayList());
        });
        List<LocalDate> dataLabels = new ArrayList<>();

        data.getDailyTransitions().forEach((k, v) -> {
            if (k.isAfter(tillDate)) {
                // TODO: change this to for loop so break could be used here....
            } else if (k.isAfter(sinceDate)) {
                dataLabels.add(k);
                statusOrder.forEach(status -> {
                    Integer newValue = statusValues.merge(status, v.getValueChangeForStatus(status), Integer::sum);
                    cfd.get(status).add(newValue);
                });
            } else {
                statusOrder.forEach(status -> {
                    statusValues.merge(status, v.getValueChangeForStatus(status), Integer::sum);
                });
//                Stream.of(doneStatuses).forEach(status -> statusValues.put(status, 0));
            }
        });
        return GraphBuilder.constructGraph("Cfd", dataLabels, cfd, true, false);
    }

}
