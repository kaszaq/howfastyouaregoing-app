package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.controllers;

import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.Chart;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableSet;
import java.io.IOException;
import static java.time.DayOfWeek.MONDAY;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import static java.time.temporal.ChronoUnit.DAYS;
import java.time.temporal.TemporalAdjuster;
import static java.time.temporal.TemporalAdjusters.nextOrSame;
import static java.time.temporal.TemporalAdjusters.previousOrSame;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.kaszaq.howfastyouaregoing.agile.AgileProject;
import pl.kaszaq.howfastyouaregoing.agile.Issue;
import static pl.kaszaq.howfastyouaregoing.agile.IssuePredicates.inStatus;
import static pl.kaszaq.howfastyouaregoing.agile.IssuePredicates.updatedAfter;
import pl.kaszaq.howfastyouaregoing.agile.IssueStatusTransition;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.Filters;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.GraphBuilder;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.ProjectKeeper;

@RestController
public class ThroughputPerTypeController {

    @Autowired
    ProjectKeeper projectKeeper;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/throughputPerTypeController.csv", produces = "text/csv")
    public String getThroughputCsv(
            @RequestParam(value = "projectId") String projectId,
            @RequestParam(value = "doneStatuses") String[] doneStatuses,
            @RequestParam(value = "throughputGroupBy") String throughputGroupBy,
            Filters filters
    ) throws JsonProcessingException, IOException {
        Set<String> finalStatus = ImmutableSet.copyOf(doneStatuses);
        Predicate<Issue> commonFilters = filters.getFilter();
        AgileProject project = projectKeeper.getProject(projectId);
        LocalDate since = ObjectUtils.max(filters.getSinceDate(), project.getFirstIssueCreateDate().toLocalDate()).with(nextOrSame(MONDAY));
        LocalDate till = filters.getTillDate();
        final Map<String, TreeMap<LocalDate, Integer>> result = getThroughputPerType(projectId, commonFilters, finalStatus, throughputGroupBy, since, filters.getRollingAverage());
        List<String> groupNames = result.keySet().stream().collect(toList());
        StringBuilder response = new StringBuilder();
        char delimeter = ';';
        response.append("Date").append(delimeter);
        groupNames.forEach(name -> response.append(name).append(delimeter));
        response.append("\r\n");
        Stream<LocalDate> dates = result.entrySet().stream().flatMap(entry -> entry.getValue().keySet().stream())
                .distinct()
                .filter(date -> !date.isBefore(since))
                .filter(date -> !date.isAfter(till))
                .sorted();
        dates.forEach(date -> {
            response.append(date).append(delimeter);
            groupNames.forEach(name -> response.append(result.get(name).getOrDefault(date, 0)).append(delimeter));
            response.append("\r\n");
        });
        return response.toString();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping("/throughputPerTypeController")
    public Chart getThroughputPerType(
            @RequestParam(value = "projectId") String projectId,
            @RequestParam(value = "doneStatuses") String[] doneStatuses,
            @RequestParam(value = "throughputGroupBy") String throughputGroupBy,
            Filters filters
    ) throws JsonProcessingException, IOException {
        Set<String> finalStatus = ImmutableSet.copyOf(doneStatuses);
        Predicate<Issue> commonFilters = filters.getFilter();
// todo rewrite these streams to be as they are in the csv version

        AgileProject project = projectKeeper.getProject(projectId);
        LocalDate since = ObjectUtils.max(filters.getSinceDate(), project.getFirstIssueCreateDate().toLocalDate()).with(nextOrSame(MONDAY));
        LocalDate till = filters.getTillDate();
        final Map<String, TreeMap<LocalDate, Integer>> result = getThroughputPerType(projectId, commonFilters, finalStatus, throughputGroupBy, since, filters.getRollingAverage());
        List<LocalDate> dates = result.entrySet().stream().flatMap(entry -> entry.getValue().keySet().stream())
                .distinct()
                .filter(date -> !date.isBefore(since))
                .filter(date -> !date.isAfter(till))
                .sorted().collect(Collectors.toList());
        Map<String, List<Integer>> typesData = new TreeMap<>();
        final Optional<LocalDate> latesDateOpt = result.values().stream().map(map -> map.lastKey()).max(LocalDate::compareTo);
        if (latesDateOpt.isPresent()) {

            int labelsSize = dates.size();
            result.keySet().forEach(key -> typesData.put(key, new ArrayList<>(labelsSize)));
            dates.forEach((label) -> {
                result.entrySet().forEach((entry) -> {
                    typesData.get(entry.getKey()).add(Optional.ofNullable(entry.getValue().get(label)).orElse(0));
                });
            });
        }
        return GraphBuilder.constructGraph(
                "Throughput per type",
                dates,
                typesData,
                true,
                false);

    }

    private Map<String, TreeMap<LocalDate, Integer>> getThroughputPerType(
            String projectId,
            Predicate<Issue> commonFilters,
            Set<String> finalStatus,
            String throughputGroupBy,
            LocalDate since,
            int daysAverage) {
        Function<Issue, String> groupingFunct;
        if ("type".equals(throughputGroupBy)) {
            groupingFunct = Issue::getType;
        } else if ("reporters".equals(throughputGroupBy)) {
            groupingFunct = Issue::getCreator;
        } else if ("resolutions".equals(throughputGroupBy)) {
            groupingFunct = i -> i.getResolution() == null ? "unknown" : i.getResolution();
        } else {
            groupingFunct = Issue::getType;
        }
        LocalDate now = LocalDate.now();
        return projectKeeper.getProject(projectId).getAllIssues().stream()
                .filter(commonFilters)
                .filter(inStatus(finalStatus).and(updatedAfter(since.atStartOfDay(ZoneId.systemDefault()))))
                .filter(i -> i.getIssueStatusTransitions().stream().anyMatch(st -> finalStatus.contains(st.getToStatus())))
                .collect(
                        groupingBy(groupingFunct,
                                Collectors.toMap(i -> getClosedDate(i, finalStatus).with(calcBusketDate(now, daysAverage)),
//                                Collectors.toMap(i -> getClosedDate(i, finalStatus).with(previousOrSame(MONDAY)),
                                        i -> 1,
                                        Integer::sum,
                                        TreeMap::new
                                )
                        )
                );
    }
public static TemporalAdjuster calcBusketDate(LocalDate now, int daysAverage) {

        return (temporal) -> {
            long diff = DAYS.between(temporal, now)/daysAverage * daysAverage;
            return now.minusDays(diff);
        };
    }

    private LocalDate getClosedDate(Issue i, Set<String> finalStatuses) {
        LocalDate returnDate = null;
        boolean previousWasClosed = false;
        for (IssueStatusTransition issueStatusTransition : i.getIssueStatusTransitions()) {
            if (!previousWasClosed && (finalStatuses.contains(issueStatusTransition.getToStatus()))) {
                returnDate = issueStatusTransition.getDate().toLocalDate();
                previousWasClosed = true;
            } else if (!finalStatuses.contains(issueStatusTransition.getToStatus())) {
                previousWasClosed = false;
            }
        }
        return returnDate;
    }
}
