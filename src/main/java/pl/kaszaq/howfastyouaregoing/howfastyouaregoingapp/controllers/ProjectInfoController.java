/*
 * Copyright 2017 Michal Kasza.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.controllers;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.kaszaq.howfastyouaregoing.agile.AgileProject;
import pl.kaszaq.howfastyouaregoing.agile.Issue;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.ProjectInfo;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.ProjectKeeper;

@RestController
class ProjectInfoController {

   
    @Autowired
    ProjectKeeper projectKeeper;

    @CrossOrigin(origins = "*")
    @GetMapping("/projectInfoController")
    public ProjectInfo getStatuses(@RequestParam(value = "projectId") String projectId) {
        
        final AgileProject project = projectKeeper.getProject(projectId);
        
        final Collection<Issue> issues = project.getAllIssues();
        List<String> statuses = project.getProbableStatusOrder();

        List<String> labels = issues.stream()
                .flatMap(i -> i.getLabels().stream()).distinct().collect(Collectors.toList());
        List<String> components = issues.stream()
                .flatMap(i -> i.getComponents().stream()).distinct().collect(Collectors.toList());

        List<String> resolutions = issues.stream()
                .map(i -> i.getResolution()).distinct().filter(r -> r != null).collect(Collectors.toList());
        
        List<String> reporters = issues.stream()
                .map(i -> i.getCreator()).distinct().filter(r -> r != null).collect(Collectors.toList());
//        List<String> assigners = issues.stream()
//                .map(i -> i.get).distinct().filter(r -> r != null).collect(Collectors.toList());
        List<String> types = issues.stream()
                .map(i -> i.getType()).distinct().filter(r -> r != null).collect(Collectors.toList());
        ProjectInfo projectInfo = new ProjectInfo(statuses, labels, components, resolutions, reporters, types);
        return projectInfo;
    }

}
