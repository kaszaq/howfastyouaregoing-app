package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.controllers;

import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.Chart;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.LineChart;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.color.Color;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.datapoint.LocalDateDataPoint;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.dataset.LineDataset;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.scales.LinearScale;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.be.ceau.chart.options.scales.ScaleLabel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.io.IOException;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.ObjectUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.kaszaq.howfastyouaregoing.agile.AgileProject;
import pl.kaszaq.howfastyouaregoing.agile.Issue;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.Filters;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.chart.GraphBuilder;
import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.NumberUtils;

import pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp.ProjectKeeper;

import pl.kaszaq.howfastyouaregoing.cfd.CfdData;
import pl.kaszaq.howfastyouaregoing.cfd.CfdDataComputer;

@RestController
public class MonteCarloController {

    @Autowired
    ProjectKeeper projectKeeper;

    @CrossOrigin(origins = "*")
    @RequestMapping("/monteCarloController")
    public Chart getMonteCarlo(
            @RequestParam(value = "projectId") String projectId,
            @RequestParam(value = "doneStatuses") String[] doneStatuses,
            @RequestParam(value = "monteCarloPredictions") int monteCarloPredictions,
            Filters filters
    ) throws JsonProcessingException, IOException {

        Set<String> finalStatus = ImmutableSet.copyOf(doneStatuses);
        int daysInWeek = 7;
        Predicate<Issue> commonFilters = filters.getFilter();
//                inResolution("Won't Fix", "Cannot Reproduce", "Duplicate", "Incomplete", "Not an Issue", "Not Enough Information", "Retest", "Unresolved").negate()
//                .and(isEpic().negate())
//                .and(hasSubtasks().negate());

        final AgileProject project = projectKeeper.getProject(projectId);
        LocalDate since = ObjectUtils.max(filters.getSinceDate(), project.getFirstIssueCreateDate().toLocalDate());;
        LocalDate till = filters.getTillDate();
        return doTheMagic(project, finalStatus, since, till, commonFilters, monteCarloPredictions);
    }

    private Chart doTheMagic(AgileProject project, Set<String> finalStatus, final LocalDate since, final LocalDate till, Predicate<Issue> commonFilters, int monteCarloPredictions) throws IOException {

        final int totalTries = 1000;
        CfdData data = CfdDataComputer.calculateCfdData(project, commonFilters);

        SortedMap<LocalDate, Integer> throughput = new TreeMap<>();
        SortedMap<LocalDate, CfdData.DailyStatusChanges> dailyTransitions = data.getDailyTransitions();
        for (LocalDate k = since; !k.isAfter(till); k = k.plusWeeks(1)) {
            throughput.put(k, dailyTransitions.subMap(k.minusWeeks(1), k.plusDays(1)).values().stream()
                    .mapToInt(val -> finalStatus.stream().mapToInt(status -> val.getValueChangeForStatus(status)).sum())
                    .sum());
        }
        Integer[] througputArray = throughput.values().toArray(new Integer[throughput.size()]);

        TreeMap<LocalDate, Integer> monteCarloHistogram = new TreeMap<>();
        if (throughput.values().stream().mapToInt(v -> v).sum() != 0) {

            for (int i = 0; i < totalTries; i++) {
                int weeks = runSimulator(througputArray, monteCarloPredictions);
                monteCarloHistogram.merge(till.plusWeeks(weeks), 1, Integer::sum);
            }
        }

        List<LocalDate> labels = Stream.iterate(since, date -> date.plusWeeks(1))
                .limit(ChronoUnit.WEEKS.between(since, monteCarloHistogram.isEmpty() ? till : monteCarloHistogram.lastKey()))
                .collect(Collectors.toList());

        List<Integer> summedThroughput = throughput.values().stream().collect(() -> new ArrayList<>(), (r, v) -> {
            if (r.isEmpty()) {
                r.add(v);
            } else {
                r.add(r.get(r.size() - 1) + v);
            }
        }, ArrayList::addAll);
        final LineChart chart = GraphBuilder.constructGraph(
                "Monte Carlo Predictions",
                labels,
                ImmutableMap.of(
                        "Issues done", summedThroughput
                ),
                false);
        int currentTry = 0;
        LocalDateDataPoint startingPredictionsPoint
                = new LocalDateDataPoint(throughput.lastKey(), summedThroughput.get(summedThroughput.size() - 1).doubleValue());
        double endValue = startingPredictionsPoint.getY() + monteCarloPredictions;
        int max = monteCarloHistogram.values().stream().mapToInt(v -> v).max().orElse(0);
        List<LocalDateDataPoint> histogramData = new ArrayList<>();
        for (Map.Entry<LocalDate, Integer> entry : monteCarloHistogram.entrySet()) {
            final Integer value = entry.getValue();
            histogramData.add(new LocalDateDataPoint(entry.getKey(), entry.getValue().doubleValue()));
            currentTry += value;
            if (currentTry != totalTries) {
                Color color = new Color((int) (220. * (1. - (double) value / max)), (int) (220. * ((double) value / max)), 50);
                chart.getData().addDataset(new LineDataset()
                        .setLabel(NumberUtils.prettyPrint((double) currentTry / totalTries * 100) + "%\t")
                        .setBackgroundColor(new Color(color, 0.8))
                        .setBorderColor(color)
                        .setBorderWidth(1)
                        .setFill(false)
                        .setType("line")
                        .setData(startingPredictionsPoint, new LocalDateDataPoint(entry.getKey(), endValue))
                );
            }

//            }
        }
        Color barColor = Color.BLUE_VIOLET;

        chart.getData().addDataset(new LineDataset()
                .setType("bar")
                .setLabel("Histogram")
                .setBackgroundColor(barColor)
                .setData(histogramData)
                .setYAxisID("y-axis-2")
        );
        chart.setType("bar");
        chart.getOptions().getTooltips().setMode("nearest");
        chart.getOptions().getScales().addyAxis(new LinearScale()
                .setDisplay(false)
                .setScaleLabel(new ScaleLabel()
                        .setDisplay(true)
                        .setLabelString("Value")
                )
                .setId("y-axis-2")
        );
        return chart;

    }

    public static TreeMap<LocalDate, Map<String, Integer>> getCfdData(CfdData data) {
        Map<String, Integer> statusValues = new HashMap<>();
        TreeMap<LocalDate, Map<String, Integer>> cfdStatus = new TreeMap<>();
        List<String> statuses = data.getDailyTransitions().values().stream().flatMap(tr -> tr.getStatusChanges().keySet().stream()).distinct().filter(s -> s != null).collect(Collectors.toList());
        data.getDailyTransitions().forEach((k, v) -> {
            HashMap<String, Integer> map = new HashMap<>();
            cfdStatus.put(k, map);
            statuses.forEach(status -> {
                Integer newValue = statusValues.merge(status, v.getValueChangeForStatus(status), Integer::sum);
                map.put(status, newValue);
            });
        });
        return cfdStatus;
    }

    private int runSimulator(Integer[] througputArray, int numberOfIssuesToPredictIn6MonthsTime) {
        int weeks = 0;
        int doneIssues = 0;
        while (doneIssues < numberOfIssuesToPredictIn6MonthsTime) {
            weeks++;
            doneIssues += througputArray[(int) (Math.random() * througputArray.length)];
        }
        return weeks;
    }
}
