package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.kaszaq.howfastyouaregoing.agile.AgileProject;
import pl.kaszaq.howfastyouaregoing.agile.AgileProjectConfiguration;
import pl.kaszaq.howfastyouaregoing.agile.AgileProjectProvider;
import pl.kaszaq.howfastyouaregoing.agile.jira.JiraAgileProjectProviderBuilderFactory;
import pl.kaszaq.howfastyouaregoing.storage.FileStorage;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class ProjectStorageImpl implements ProjectStorage {

    @Value("${storage.dir}")
    private String storageDir;
    private File cacheDir;
    @Autowired
    FileStorage fileStorage;
    @Value("${jira.url}")
    private String jiraUrl;


    AgileProjectProvider localProjectProvider;
    private final AgileProjectConfiguration agileProjectConfiguration = AgileProjectConfiguration.builder().build();
    private LoadingCache<String, AgileProject> projectsCache;

    @Override
    public SortedMap<String, ProjectStatus> getAvailableProjects() {
        String jiraUrl = getCurrentUserJiraUrl();
        return getAvailableProjects(jiraUrl);
    }

    private SortedMap<String, ProjectStatus> getAvailableProjects(String jiraUrl) {
        //TODO: for now just read from filesystem
        return new TreeMap(Stream.<File>of(cacheDir
                .listFiles((File file) -> file.isFile() && file.getName().endsWith(".json")))
                .map(file -> file.getName().substring(0, file.getName().indexOf(".json")))
                .collect(Collectors.toMap(c -> c, c -> new ProjectStatus( true))));
    }



    @Override
    public AgileProject getProject(String projectId) {
        String jiraUrl = getCurrentUserJiraUrl();
        return getProject(projectId, jiraUrl);
    }

    private AgileProject getProject(String projectId, String jiraUrl) {

        return null;
    }


    private String getCurrentUserJiraUrl() {
        return jiraUrl;
    }
    @PostConstruct
    public void setupCache() {
        System.out.println("Storage dir: " + storageDir);
        cacheDir = new File(storageDir);
        System.out.println("cache dir: " + cacheDir);

        localProjectProvider = JiraAgileProjectProviderBuilderFactory
                .withJsession(null)
                .withCacheDir(cacheDir)
                .withFileStorage(fileStorage)
                .withCacheOnly(true)
                .withJiraUrl(getCurrentUserJiraUrl())
                .withEmptyDescriptionAndSummary(true)
                .withCacheRawJiraFiles(false)
                .build();
        projectsCache = CacheBuilder.newBuilder().expireAfterAccess(120, TimeUnit.SECONDS)
                .build(new CacheLoader<String, AgileProject>() {
                    public AgileProject load(String projectId) throws RuntimeException {
                        log.info("Initial loading project {} from file system.", projectId);
                        final Optional<AgileProject> projectFromFile = localProjectProvider.loadProject(projectId, agileProjectConfiguration);
                        if (projectFromFile.isPresent()) {
                            return projectFromFile.get();
                        } else {
                            log.warn("Initial loading project {} from file system failed.", projectId);
                            throw new RuntimeException("Initial loading project from file system failed: " + projectId);
                        }
                    }
                });
    }
}
