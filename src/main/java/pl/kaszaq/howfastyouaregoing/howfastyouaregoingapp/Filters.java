/*
 * Copyright 2017 Michal Kasza.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.kaszaq.howfastyouaregoing.howfastyouaregoingapp;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import java.util.Collection;
import static java.util.Collections.emptySet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import pl.kaszaq.howfastyouaregoing.agile.Issue;
import pl.kaszaq.howfastyouaregoing.agile.IssuePredicates;
import static pl.kaszaq.howfastyouaregoing.agile.IssuePredicates.*;
import static pl.kaszaq.howfastyouaregoing.utils.CommonPredicates.alwaysTrue;

public class Filters {

    Set<String> statuses = emptySet();
    Set<String> labels = emptySet();
    Set<String> components = emptySet();
    Set<String> resolutions = emptySet();
    Set<String> reporters = emptySet();
    Set<String> types = emptySet();
    Set<String> statusesRequired = emptySet();
    Set<String> labelsRequired = emptySet();
    Set<String> componentsRequired = emptySet();
    Set<String> resolutionsRequired = emptySet();
    Set<String> reportersRequired = emptySet();
    Set<String> typesRequired = emptySet();
    boolean filterIssuesWithSubtasks;
    boolean filterIssuesWithoutSubtasks;
    private Predicate<Issue> filter;
    private int rollingAverage = 14;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate sinceDate = null;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate tillDate = LocalDate.now();

    public void setSinceDate(LocalDate sinceDate) {
//        this.sinceDate = sinceDate;
    }

    public void setTillDate(LocalDate tillDate) {
//        this.tillDate = tillDate;
    }

    public void setDateLimit(int months) {
        
        if (months > 0) {
            sinceDate = LocalDate.now().minusMonths(months);//.with(firstDayOfMonth());
        }
    }

    public Set<String> getStatuses() {
        return statuses;
    }

    public void setStatuses(Set<String> statuses) {
        this.statuses = statuses;
    }

    public Set<String> getLabels() {
        return labels;
    }

    public void setLabels(Set<String> labels) {
        this.labels = labels;
    }

    public Set<String> getComponents() {
        return components;
    }

    public void setComponents(Set<String> components) {
        this.components = components;
    }

    public Set<String> getResolutions() {
        return resolutions;
    }

    public void setResolutions(Set<String> resolutions) {
        this.resolutions = resolutions;
    }

    public int getRollingAverage() {
        return rollingAverage;
    }

    public void setRollingAverage(int rollingAverage) {
        this.rollingAverage = rollingAverage;
    }

    public Set<String> getReporters() {
        return reporters;
    }

    public void setReporters(Set<String> reporters) {
        this.reporters = reporters;
    }

    public Set<String> getTypes() {
        return types;
    }

    public void setTypes(Set<String> types) {
        this.types = types;
    }

    public boolean isFilterIssuesWithSubtasks() {
        return filterIssuesWithSubtasks;
    }

    public void setFilterIssuesWithSubtasks(boolean filterIssuesWithSubtasks) {
        this.filterIssuesWithSubtasks = filterIssuesWithSubtasks;
    }

    public boolean isFilterIssuesWithoutSubtasks() {
        return filterIssuesWithoutSubtasks;
    }

    public void setFilterIssuesWithoutSubtasks(boolean filterIssuesWithoutSubtasks) {
        this.filterIssuesWithoutSubtasks = filterIssuesWithoutSubtasks;
    }

    public Set<String> getStatusesRequired() {
        return statusesRequired;
    }

    public void setStatusesRequired(Set<String> statusesRequired) {
        this.statusesRequired = statusesRequired;
    }

    public Set<String> getLabelsRequired() {
        return labelsRequired;
    }

    public void setLabelsRequired(Set<String> labelsRequired) {
        this.labelsRequired = labelsRequired;
    }

    public Set<String> getComponentsRequired() {
        return componentsRequired;
    }

    public void setComponentsRequired(Set<String> componentsRequired) {
        this.componentsRequired = componentsRequired;
    }

    public Set<String> getResolutionsRequired() {
        return resolutionsRequired;
    }

    public void setResolutionsRequired(Set<String> resolutionsRequired) {
        this.resolutionsRequired = resolutionsRequired;
    }

    public Set<String> getReportersRequired() {
        return reportersRequired;
    }

    public void setReportersRequired(Set<String> reportersRequired) {
        this.reportersRequired = reportersRequired;
    }

    public Set<String> getTypesRequired() {
        return typesRequired;
    }

    public void setTypesRequired(Set<String> typesRequired) {
        this.typesRequired = typesRequired;
    }

    public Predicate<Issue> getFilter() {
        if (filter == null) {
            Predicate<Issue> newFilter
                    = inStatus(statuses).negate()
                            .and(inResolution(resolutions).negate())
                            .and(reportedBy(reporters).negate())
                            .and(isType(types).negate())
                            .and(ifNotEmptyApply(IssuePredicates::inStatus, statusesRequired))
                            .and(ifNotEmptyApply(IssuePredicates::inResolution, resolutionsRequired))
                            .and(ifNotEmptyApply(IssuePredicates::reportedBy, reportersRequired))
                            .and(ifNotEmptyApply(IssuePredicates::isType, typesRequired))
                            .and(ifNotEmptyApply(IssuePredicates::hasAnyLabels, labelsRequired))
                            .and(ifNotEmptyApply(IssuePredicates::hasAnyComponents, componentsRequired));

            if (!labels.isEmpty()) {
                newFilter = newFilter.and(hasAnyLabels(labels).negate());
            }
            if (!components.isEmpty()) {
                newFilter = newFilter.and(hasAnyComponents(components).negate());
            }
            if (filterIssuesWithSubtasks) {
                newFilter = newFilter.and(hasSubtasks().negate());
            }
            if (filterIssuesWithoutSubtasks) {
                newFilter = newFilter.and(hasSubtasks());
            }
            filter = newFilter;
        }
        return filter;
    }

    public LocalDate getSinceDate() {
        return sinceDate;
    }

    public LocalDate getTillDate() {
        return tillDate;
    }

    private <T extends Collection> Predicate<Issue> ifNotEmptyApply(Function<T, Predicate<Issue>> func, T val) {
        if (!val.isEmpty()) {
            return func.apply(val);
        } else {
            return alwaysTrue();
        }
    }
}
