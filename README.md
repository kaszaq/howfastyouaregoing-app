# Running
Simplest way to run this application is by using docker.

##### Example for Windows
```
docker run --rm -it --name howfastyouaregoing ^
  -e JIRA_URL=https://yourname.atlassian.net ^
  -e SECURITY_ENABLED=true ^
  -p 8080:8080 ^
  -v %cd%/storage:/storage ^
  kaszaq/howfastyouaregoing
```

##### Example for Unix
```
docker run --rm -it --name howfastyouaregoing \
  -e JIRA_URL=https://yourname.atlassian.net \
  -e SECURITY_ENABLED=true \
  -p 8080:8080 \
  -v $(pwd)/storage:/storage \
  kaszaq/howfastyouaregoing
```

## Storage
You need to add volume for directory `/storage`. If you do not set any volume, the projects data will not be persisted and will force you to download it from Jira every time you use this app.

## Configuration parameters
To run application you need to specify some parameters. These can be provided as system properties or enviroment variables.
Following information takes you provide them as enviroment variables to docker image.

### Required parameters
* `JIRA_URL` - url to your jira, for instance to cloud jira it would something like `https://yourname.atlassian.net`

### Optional parameters
* `ENCRYPTION_PASSWORD` - when provided all stored files will be encrypted using this password.

##### Running on single user
Howfastyouaregoing app can run on one user account - all  information will be downloaded from Jira using that user credentials. Moreover project data will be automatically refreshed against Jira every 6 hours.
* `JIRA_USERNAME` - username of user to be used
* `JIRA_PASSWORD` - this is either password or [api token](https://confluence.atlassian.com/cloud/api-tokens-938839638.html)

##### Running with user authentication
* `SECURITY_ENABLED` (`true` / `false`, default `false`) - enabling security will add authentication. Authentication will be performed against Jira instance - so users will need to provide credentials they use to authenticate to Jira instance. If authentication is federated, this should not be enabled. If left to `false` and no single user is setup, application will be able to only use currently downloaded project information. If set to `true` and no single user is setup projects will be downloaded only during given user session on his behalf. Project information will not be automatically refreshed in the background.

### Other

* `PROXY` - set to "`-Dhttp.proxyHost=proxyaddress.here -Dhttp.proxyPort=80 -Dhttps.proxyHost=proxyaddress.here -Dhttps.proxyPort=443`" if need to use proxy to access jira