FROM openjdk:8-jre-alpine
ARG BUILD_DATE
ARG VERSION=DIRTY
ARG VCS_URL=??
ARG VCS_REF=??
LABEL org.label-schema.schema-version="1.0" \
  org.label-schema.name="howfastyouaregoing-app"\
  org.label-schema.description="Application used to visualise metrics of Jira Projects"\
  org.label-schema.url="http://howfastyouaregoing.com"\
  org.label-schema.vendor="kaszaq.pl"\
  org.label-schema.maintainer="kaszaq@gmail.com" \
  org.label-schema.build-date="$BUILD_DATE" \
  org.label-schema.vcs-ref="$VCS_REF" \
  org.label-schema.vcs-url="$VCS_URL" \
  org.label-schema.version="$VERSION" \
  org.label-schema.docker.cmd="docker run --rm --name hfyag -it -p 8080:8080 -v %cd%/storage:/storage -e JIRA_URL=https://jira.example.com -e STORAGE_DIR=/storage myapp"   

# Set URANDOM
RUN sed \
 -e 's#securerandom.source=file:/dev/random#securerandom.source=file:/dev/urandom#' \
 -i $JAVA_HOME/lib/security/java.security

ENV PROXY=""
RUN mkdir /storage
RUN chown nobody:nogroup /storage
VOLUME /storage
ENV STORAGE_DIR="/storage"

COPY target/howfastyouaregoing-app*.jar howfastyouaregoing-app.jar
COPY startUp.sh startUp.sh
RUN chmod +x startUp.sh
USER 65534
EXPOSE 8080
CMD ./startUp.sh
